#ifndef CHECKERBOARD_HPP_
#define CHECKERBOARD_HPP_
#include <opencv2/opencv.hpp>
#include <string>
namespace checkerboard {
    cv::Mat create_checkerboard(int width, int height, int size);
    void display_until_esc(const std::string & window_name, const cv::Mat & image);
}
#endif

