(define-module (checkerboard)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (guix gexp) 
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages pkg-config)
  #:use-module (opencv))

(define-public checkerboard
  (package
    (name "checkerboard")
    (version "0.1")
    (source (local-file "./checkerboard" #:recursive? #t))
    (build-system cmake-build-system)
    (arguments '(#:tests? #f)) 
    (native-inputs `(("pkg-config" ,pkg-config)))
    (inputs `(("opencv" ,opencv)))
    (synopsis "checkerboard")
    (description "Checkerboard.")
    (home-page "http://www.example.org")
    (license public-domain)))

(define python-pycheckerboard
  (package
    (name "python-pycheckerboard")
    (version "0.1")
    (source (local-file "./pycheckerboard" #:recursive? #t))
    (arguments '(#:tests? #f)) 
    (build-system python-build-system)
    (inputs `(("checkerboard" ,checkerboard)
              ("boost" ,boost)
              ("opencv" ,opencv)))
    (synopsis "pycheckerboard")
    (description "Pycheckerboard.")
    (home-page "http://www.example.org")
    (license public-domain)))

(define-public python2-pycheckerboard
  (package-with-python2 python-pycheckerboard))


