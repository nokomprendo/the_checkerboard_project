from setuptools import setup, Extension

checkerboard_module = Extension('checkerboard_binding',
    sources = ['checkerboard_binding/checkerboard_binding.cpp'],
    libraries = ['checkerboard', 'boost_python', 'opencv_core', 'opencv_highgui'])

setup(name = 'pycheckerboard',
    version = '0.1',
    packages = ['pycheckerboard'],
    python_requires = '<3',
    ext_modules = [checkerboard_module])

