#!/usr/bin/env python

import checkerboard_binding as cb

def test1():
    print('running test1.py...')
    img = cb.create_checkerboard(400, 300, 10)
    cb.display_until_esc("pycheckerboard", img)

if __name__ == '__main__':
    test1()

