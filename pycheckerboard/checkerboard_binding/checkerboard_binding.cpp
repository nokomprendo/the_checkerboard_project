#include <checkerboard.hpp>
#include <boost/python.hpp>
using namespace boost::python;
BOOST_PYTHON_MODULE( checkerboard_binding ) {
    class_<cv::Mat>("Mat");
    def("create_checkerboard", &checkerboard::create_checkerboard);
    def("display_until_esc", &checkerboard::display_until_esc);
}
